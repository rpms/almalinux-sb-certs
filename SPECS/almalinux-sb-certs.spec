%define pki %{_datadir}/pki/sb-certs/

Name: almalinux-sb-certs
Epoch: 1
Version: 1.0
Release: 1%{?dist}
Summary: AlmaLinux certificates for the Secure Boot

License: GPLv2
Group: System Environment/Kernel
Provides: system-sb-certs = %{epoch}:%{version}-%{release}

Source0: clsecureboot001.cer

%define secureboot_ca_0 %{SOURCE0}
%define secureboot_key_0 %{SOURCE0}

%description
AlmaLinux certificates for the Secure Boot

%install
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf "$RPM_BUILD_ROOT"

install -d $RPM_BUILD_ROOT/%{pki}
install %{secureboot_ca_0} $RPM_BUILD_ROOT/%{pki}/secureboot-ca-%{_arch}.cer
install %{secureboot_key_0} "$RPM_BUILD_ROOT/%{pki}/secureboot-kernel-%{_arch}.cer"

%files
%defattr(-,root,root)
%{pki}/*

%changelog
* Thu Nov 03 2022 Andrew Lukoshko <alukoshko@almalinux.org> - 1.0-1
- Initial release
